/**
Stack Overflow question: https://stackoverflow.com/q/53700781/272735

I have the following function `findFirstDuplicateFrequency` that implements (correctly) an algorithm for a programming [puzzle](https://adventofcode.com/2018/day/1).

Instead of imperative looping I'd like to promote D's [functional features](https://tour.dlang.org/tour/en/gems/functional-programming) and thought I can apply [reduce](https://dlang.org/phobos/std_algorithm_iteration.html#.reduce) to the problem.

I run into an issues that I need to be able to iterate the input sequence multiple (but unknown) times and quit the processing when the exit condition is met.

AFAICS the standard reduce can't be exited in the middle of the processing and I also struggle how to carry on extra accumulator information that is used to calculate the exit condition.

So what would be the correct (?) idiomatic D approach to solve the problem in a (more) functional way ?

This is my very first D program ever and so all other comments are welcome too !

From: https://adventofcode.com/2018/day/1

You notice that the device repeats the same frequency change list over and
over. To calibrate the device, you need to find the first frequency it reaches
twice.

For example, using the same list of changes above, the device would loop as
follows:

    Current frequency  0, change of +1; resulting frequency  1.
    Current frequency  1, change of -2; resulting frequency -1.
    Current frequency -1, change of +3; resulting frequency  2.
    Current frequency  2, change of +1; resulting frequency  3.
    (At this point, the device continues from the start of the list.)
    Current frequency  3, change of +1; resulting frequency  4.
    Current frequency  4, change of -2; resulting frequency  2, which has already been seen.

In this example, the first frequency reached twice is 2. Note that your device
might need to repeat its list of frequency changes many times before a
duplicate frequency is found, and that duplicates might be found while in the
middle of processing the list.

Here are other examples:

    +1, -1 first reaches 0 twice.
    +3, +3, +4, -2, -4 first reaches 10 twice.
    -6, +3, +8, +5, -6 first reaches 5 twice.
    +7, +7, -2, -7, -4 first reaches 14 twice.

What is the first frequency your device reaches twice?
*/
int findFirstDuplicateFrequency(int[] frequencyChanges)
pure
{
  int[int] alreadySeen = [0:1];
  int frequency = 0;

 out_: while(true) {
    foreach(change; frequencyChanges) {
      frequency += change;
      if (int* _ = frequency in alreadySeen) {
        break out_;
      } else {
        alreadySeen[frequency] = 1;
      }
    }
  }

  return frequency;
} unittest {
  import std.conv : to;

  int answer = 0;

  answer = findFirstDuplicateFrequency([1, -2, 3, 1]);
  assert(answer == 2, "Got: " ~ to!string(answer));

  answer = findFirstDuplicateFrequency([1, -1]);
  assert(answer == 0, "Got: " ~ to!string(answer));

  answer = findFirstDuplicateFrequency([3, 3, 4, -2, -4]);
  assert(answer == 10, "Got: " ~ to!string(answer));

  answer = findFirstDuplicateFrequency([-6, 3, 8, 5, -6]);
  assert(answer == 5, "Got: " ~ to!string(answer));

  answer = findFirstDuplicateFrequency([7, 7, -2, -7, -4]);
  assert(answer == 14, "Got: " ~ to!string(answer));
}

/**
Even according to @AdamD.Ruppe there is no great hopes to make the code more "functional" I was inspired by @BioTronic's `cumulativeFold` + `until` hint and decided to have a second look.

Unfortunately I see no way to apply `cumulativeFold` + `until` as I don't have a sentinel value required by `until` and there is still the same stopping problem than with `reduce`.

When I was browsing [standard runtime library reference](https://dlang.org/phobos/index.html) I noticed [`each`](https://dlang.org/library/std/algorithm/iteration/each.html) do have an early exit (aka partial iteration) option. I also run into [`std.range.cycle`](https://dlang.org/library/std/range/cycle.html#0).

Combining these two shiny new things I came to the solution below. `findFirstDuplicateFrequencyV2` uses `cycle` and `each` to replace the imperative loops of the first version. I'm not sure if this version is any simpler but hopefully it is  more "trendy" !
 */
int findFirstDuplicateFrequencyV2(int[] frequencyChanges)
    pure
    {
      import std.algorithm.iteration : each;
      import std.range : cycle;
      import std.typecons : Yes, No;
    
      int[int] alreadySeen = [0:1];
      int frequency = 0;
    
      frequencyChanges.cycle.each!((change) {
          frequency += change;
          if (frequency in alreadySeen) {
            return No.each;
          }
          alreadySeen[frequency] = 1;
          return Yes.each;
        });
    
      return frequency;
    } unittest {
      import std.conv : to;
    
      int answer = 0;
    
      answer = findFirstDuplicateFrequencyV2([1, -2, 3, 1]);
      assert(answer == 2, "Got: " ~ to!string(answer));
    
      answer = findFirstDuplicateFrequencyV2([1, -1]);
      assert(answer == 0, "Got: " ~ to!string(answer));
    
      answer = findFirstDuplicateFrequencyV2([3, 3, 4, -2, -4]);
      assert(answer == 10, "Got: " ~ to!string(answer));
    
      answer = findFirstDuplicateFrequencyV2([-6, 3, 8, 5, -6]);
      assert(answer == 5, "Got: " ~ to!string(answer));
    
      answer = findFirstDuplicateFrequencyV2([7, 7, -2, -7, -4]);
      assert(answer == 14, "Got: " ~ to!string(answer));
    }
