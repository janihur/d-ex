// Integer sequences - Recamán's sequence
// https://oeis.org/A005132
//
// a(0) = 0;
// for n > 0,
//   a(n) = a(n-1) - n if positive and not already in the sequence,
//   a(n) = a(n-1) + n otherwise

import std.stdio;

// returns a single value
ulong recaman(ulong n)
  pure @safe
{
  uint[ulong] alreadySeen;

  ulong impl(ulong n) {
    if (n == 0) return 0;

    ulong prev = impl(n - 1);

    // is positive ? note integer under/overflow wraparounds
    bool isPositive = prev > n;

    // already in the sequence ?
    bool inSequence = false;
    if (uint* _ = (prev - n) in alreadySeen) {
      inSequence = true;
    }

    ulong next;
    if (isPositive && !inSequence) {
      next = prev - n;
    } else {
      next = prev + n;
    }

    alreadySeen[next] = 1;

    // debug writefln("(n = %s)(prev = %s)(isPositive = %s)(next = %s)(alreadySeen = %s)", n, prev, isPositive, next, alreadySeen);

    return next;
  }

  return impl(n);  
} unittest {
  assert(recaman(0) == 0);
  assert(recaman(1) == 1);
  assert(recaman(2) == 3);
  assert(recaman(3) == 6);
  assert(recaman(4) == 2);
  assert(recaman(5) == 7);
  assert(recaman(6) == 13);
  assert(recaman(7) == 20);
  assert(recaman(8) == 12);
  assert(recaman(9) == 21);
  assert(recaman(10) == 11);
}

// return a sequence up to n
// version 1 - recursion
ulong[] recamanL(ulong n)
  pure @safe
{
  ulong[] sequence = [0];
  uint[ulong] alreadySeen;

  ulong impl(ulong n) {
    if (n == 0) return 0;

    ulong prev = impl(n - 1);

    // is positive ? note integer under/overflow wraparounds
    bool isPositive = prev > n;

    // already in the sequence ?
    bool inSequence = false;
    if (uint* _ = (prev - n) in alreadySeen) {
      inSequence = true;
    }

    ulong next;
    if (isPositive && !inSequence) {
      next = prev - n;
    } else {
      next = prev + n;
    }

    sequence ~= next;
    alreadySeen[next] = 1;

    // debug writefln("(n = %s)(prev = %s)(isPositive = %s)(next = %s)(alreadySeen = %s)", n, prev, isPositive, next, alreadySeen);

    return next;
  }

  impl(n);
  return sequence;
} unittest {
  assert(recamanL(0) == [0]);
  assert(recamanL(10) == [0, 1, 3, 6, 2, 7, 13, 20, 12, 21, 11]);
}

// return a sequence up to n
// version 2 - loop
ulong[] recamanL2(ulong n)
  pure @safe
{
  ulong[] sequence = [0];
  uint[ulong] alreadySeen;

  foreach(i; 1 .. n + 1) {
    ulong prev = sequence[$-1];

    // is positive ? note integer under/overflow wraparounds
    bool isPositive = prev > i;

    // already in the sequence ?
    bool inSequence = false;
    if (uint* _ = (prev - i) in alreadySeen) {
      inSequence = true;
    }

    ulong next;
    if (isPositive && !inSequence) {
      next = prev - i;
    } else {
      next = prev + i;
    }

    sequence ~= next;
    alreadySeen[next] = 1;
  }
  return sequence;
} unittest {
  assert(recamanL2(0) == [0]);
  assert(recamanL2(10) == [0, 1, 3, 6, 2, 7, 13, 20, 12, 21, 11]);
}

// return a sequence up to n
// version 3 - template
T[] recamanT(T)(T n)
  pure @safe
{
  T[] sequence = [0];
  uint[T] alreadySeen;

  foreach(i; 1 .. n + 1) {
    T prev = sequence[$-1];

    // is positive ? note integer under/overflow wraparounds
    bool isPositive = prev > i;

    // already in the sequence ?
    bool inSequence = false;
    if (uint* _ = (prev - i) in alreadySeen) {
      inSequence = true;
    }

    T next;
    if (isPositive && !inSequence) {
      next = prev - i;
    } else {
      next = prev + i;
    }

    sequence ~= next;
    alreadySeen[next] = 1;
  }
  return sequence;
} unittest {
  assert(recamanT(0) == [0]);
  assert(recamanT(10) == [0, 1, 3, 6, 2, 7, 13, 20, 12, 21, 11]);
}

// golfed variations ------------------------------------------------------------
// https://codegolf.stackexchange.com/a/188759/88197

private:

// recursion - 166 chars
int[]r1(int n){
int[]t=[0];
int[int]s;
int f(int n){
if(n<1)return 0;
int p=f(n-1);
bool a=p>n;
bool b=(p-n)in s?1:0;
int c=a&&!b?p-n:p+n;
t~=c;
s[c]=1;
return c;}
f(n);
return t;}
unittest {
  assert(r1(0) == [0]);
  assert(r1(10) == [0, 1, 3, 6, 2, 7, 13, 20, 12, 21, 11]);
}
// int[]r(int n){int[]t=[0];int[int]s;int f(int n){if(n<1)return 0;int p=f(n-1);bool a=p>n;bool b=(p-n)in s?1:0;int c=a&&!b?p-n:p+n;t~=c;s[c]=1;return c;}f(n);return t;}

// loop - 117 chars
int[]r2(int n){
int[]a=[0];
int[int]b;
foreach(i;1..n+1){
int x=a[$-1];
x+=(x>i)&&!((x-i)in b)?-i:i;
a~=x;
b[x]=1;}
return a;}
unittest {
  assert(r2(0) == [0]);
  assert(r2(10) == [0, 1, 3, 6, 2, 7, 13, 20, 12, 21, 11]);
}
// int[]r(int n){int[]a=[0];int[int]b;foreach(i;1..n+1){int x=a[$-1];x+=(x>i)&&!((x-i)in b)?-i:i;a~=x;b[x]=1;}return a;}
// void main(){import std.stdio;writeln(r(10));}

// template + loop - 108 chars
T[]r3(T)(T n){
T[]a=[0];
T[T]b;
foreach(i;1..n+1){
T x=a[$-1];
x+=(x>i)&&!((x-i)in b)?-i:i;
a~=x;
b[x]=1;}
return a;}
unittest {
  assert(r3(0) == [0]);
  assert(r3(10) == [0, 1, 3, 6, 2, 7, 13, 20, 12, 21, 11]);
}
// T[]r(T)(T n){T[]a=[0];T[T]b;foreach(i;1..n+1){T x=a[$-1];x+=(x>i)&&!((x-i)in b)?-i:i;a~=x;b[x]=1;}return a;}
// void main(){import std.stdio;writeln(r(10));}
