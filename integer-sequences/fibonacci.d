// Integer sequences - Fibonacci numbers
// https://oeis.org/A000045
// https://en.wikipedia.org/wiki/Fibonacci_number

// fib(94) would be out of ulong range

// recursion (inefficient due poor DMD optimizations ?)
// TODO optimised with internal cache: import std.functional.memoize
ulong fib(uint n)
  pure @safe
in {
  assert (n < 94);
} do {
  return (n < 2) ? n : fib(n - 1) + fib(n - 2);
} unittest {
  assert(fib(0)  == 0);
  assert(fib(1)  == 1);
  assert(fib(2)  == 1);
  assert(fib(3)  == 2);
  assert(fib(4)  == 3);
  assert(fib(5)  == 5);
  assert(fib(10) == 55);
  assert(fib(20) == 6765);
  assert(fib(30) == 832040);
  assert(fib(40) == 102334155);
  // disabled on purpose - takes too long in my (virtual) computer
  // assert(fib(50) == 12586269025);
}

// loop
ulong fib2(uint n)
  pure @safe
in {
  assert (n < 94);
} do {
  if (n < 2) return n;

  ulong a;
  ulong b = 1;
  ulong c;

  foreach (i; 2 .. n + 1) {
    c = a + b;
    a = b;
    b = c;
  }

  return c;
} unittest {
  assert(fib2(0)  == 0);
  assert(fib2(1)  == 1);
  assert(fib2(2)  == 1);
  assert(fib2(3)  == 2);
  assert(fib2(4)  == 3);
  assert(fib2(5)  == 5);
  assert(fib2(10) == 55);
  assert(fib2(20) == 6765);
  assert(fib2(30) == 832040);
  assert(fib2(40) == 102334155);
  assert(fib2(50) == 12586269025);
  assert(fib2(60) == 1548008755920);
  assert(fib2(70) == 190392490709135);
  assert(fib2(80) == 23416728348467685);
  assert(fib2(90) == 2880067194370816120);
  assert(fib2(93) == 12200160415121876738UL);
}

// range
// TODO constructor to initialize the starting point
struct Fib {
  private ulong a;
  private ulong b = 1;
  private ulong n;

  // TODO limit n < 94
  enum empty = false;
  
  ulong front() const @property {
    if (n < 2) return n;
    return a + b;
  }
  
  void popFront() {
    if (n < 2) { n++; return; }
    ulong c = a + b;
    a = b;
    b = c;
  }
} unittest {
  import std.array : array;
  import std.range : drop, take;

  ulong[] expected13 = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144];
  Fib fib;
  assert(fib.take(11).drop(3).array == expected13[3 .. 11]);
}
