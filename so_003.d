/*
Stack Overflow question: https://stackoverflow.com/q/54397968/272735

Compile time initialization of an associative array

According to D Language Reference [static initialization of associative arrays][staticini] an associative array (AA) can be initialized this way:

However the example doesn't compile with a reasonable recent DMD:

    $ dmd --version
    DMD64 D Compiler v2.083.0
    Copyright (C) 1999-2018 by The D Language Foundation, All Rights Reserved written by Walter Bright
    $ dmd -de -w so_003.d
    so_003.d(3): Error: non-constant expression ["foo":5L, "bar":10L, "baz":2000L]

A bit of googling seems to indicate this is a long standing bug (?) in the language:

* [Cannot initialize associative array][forum1]
* [What is the syntax for declaring a constant string[char] AA?][so1]
* [Error in Defining an associative array in D][so2]

So I know how to work around that (with a [static constructor][staticcons]). However considering the issue have existed already about 10 years is this in practice turned into a feature ?

In fact that just a prelude to my actual question:

Is it possible to initialize an associative array in compile time ?

In the example below I can initialize module level `string[] doubleUnits` with a generator function that is run in compile time (with [CTFE][ctfe]) as proofed by `pragma(msg)`. How I can do the same with an associative array ?

[ctfe]: https://tour.dlang.org/tour/en/gems/compile-time-function-evaluation-ctfe
[forum1]: https://forum.dlang.org/thread/hvr9u1$2l41$1@digitalmars.com
[so1]: https://stackoverflow.com/a/26862994/272735
[so2]: https://stackoverflow.com/q/30039153/272735
[staticcons]: https://dlang.org/spec/module.html#staticorder
[staticini]: https://dlang.org/spec/hash-map.html#static_initialization
*/

// immutable long[string] aa = [
//   "foo": 5,
//   "bar": 10,
//   "baz": 2000
// ];

// void main()
// {
//   import std.stdio : writefln;

//   writefln("(aa = %s)", aa);
// }

import std.stdio : writefln;

immutable char[] units = ['a', 'b', 'c'];

immutable string[] doubleUnits = generateDoubleUnits(units);
pragma(msg, "compile time: ", doubleUnits);

string[] generateDoubleUnits(immutable char[] units)
pure
{
  import std.format : format;
  
  string[] buffer;
  foreach(unit; units) {
    buffer ~= format("%s%s", unit, unit);
  }

  return buffer;
}

immutable int[string] doubleUnitMap;
// pragma(msg) below triggers the following compilation error:
// Error: static variable doubleUnitMap cannot be read at compile time
//        while evaluating pragma(msg, doubleUnitMap)
// pragma(msg, "compile time: ", doubleUnitMap);

shared static this() {
  doubleUnitMap = generateDoubleUnitMap(units);
}

int[string] generateDoubleUnitMap(immutable char[] units)
pure
{
  import std.format : format;
  
  int[string] buffer;
  foreach(unit; units) {
    string key = format("%s%s", unit, unit);
    buffer[key] = 1;
  }

  return buffer;
}

void main()
{
  writefln("(doubleUnits = %s)", doubleUnits);
  writefln("(doubleUnitMap = %s)", doubleUnitMap);
}
