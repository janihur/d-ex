import std.stdio : writefln;

void dump(alias variable)()
{
  import std.traits;
  writefln("(%s(%s) = %s)", variable.stringof, typeid(typeof(variable)), variable);
}

void main()
{
  // Sum of durations
  {
    import std.algorithm : fold, sum;
    import std.datetime : Duration, minutes;

    auto durations = [40.minutes, 27.minutes, 5.minutes];
    static assert(is(typeof(durations) == Duration[]));
    dump!durations;
    writefln("(durations = %s)", durations);

    // sum requires a special value for seed
    auto durationSum1 = durations.sum(Duration.zero);
    static assert(is(typeof(durationSum1) == Duration));
    dump!durationSum1;
    writefln("(durationSum1 = %s)", durationSum1);
    assert(durationSum1 == 72.minutes);

    // fold uses the first element as seed
    auto durationSum2 = durations.fold!((a, b) => a + b);
    static assert(is(typeof(durationSum2) == Duration));
    dump!durationSum2;
    writefln("(durationSum2 = %s)", durationSum2);
    assert(durationSum2 == 72.minutes);

    auto durationSumAsLongMinutes = durationSum1.total!("minutes");
    static assert(is(typeof(durationSumAsLongMinutes) == long));
    dump!durationSumAsLongMinutes;
    writefln("(durationSumAsLongMinutes = %s)", durationSumAsLongMinutes);
    assert(durationSumAsLongMinutes == 72);
  }

  // Step interval in one minute steps
  {
    import std.algorithm.iteration : each;
    import std.datetime : Duration, Interval, minutes;
    import std.datetime.date : DateTime;

    auto interval =
       Interval!DateTime(DateTime(2019, 01, 13, 14, 05, 0),
                         DateTime(2019, 01, 13, 14, 25, 0));
    dump!interval;

    // https://dlang.org/phobos/std_datetime_interval.html#.Interval.fwdRange
    auto minuteForwarder = delegate (scope const DateTime dt)
      {
       return dt + 1.minutes;
      };

    auto intervalRange = interval.fwdRange(minuteForwarder);
    // or "simplier"
    // auto intervalRange = interval.fwdRange(dt => dt + 1.minutes);
    dump!intervalRange;

    // count individual interval minutes
    int[int] minuteMap;
    dump!minuteMap;

    intervalRange.each!((dt) {
        if (dt.minute in minuteMap) {
          ++minuteMap[dt.minute];
        } else {
          minuteMap[dt.minute] = 1;
        }
      });

    dump!minuteMap;
  }
}
