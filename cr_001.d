/**
Finding min and max. 

Inspired by https://codereview.stackexchange.com/q/196771/15012
*/
unittest {
  import std.algorithm.iteration : fold, map;
  import std.algorithm.comparison : min, max;
  import std.typecons : tuple;
  import std.algorithm.searching : minElement, maxElement;
  import std.exception : assertThrown;

  // using fold with multiple functions when the type is sortable with
  // ordering operations (opCmp)
  {
    int[] test = [3, 2, 1, 5, 4];
    auto result = test.fold!(min, max);
    assert(result == tuple(1, 5));
  }

  // using fold with multiple functions when the type is sortable with
  // ordering operations (opCmp)
  {
    string test = "cbaed";
    auto result = test.fold!(min, max);
    assert(result == tuple('a', 'e'));
  }

  // without a seed an empty range will raise an exception
  {
    int[] test = [];
    assertThrown!Exception(test.fold!(min, max));
  }

  // works fine with an empty range when seeded
  {
    int[] test = [];
    auto result = test.fold!(min, max)(0, 0);
    assert(result == tuple(0, 0));
  }

  // sorting using other criteria than type's natural ordering operations
  // (opCmp)
  // e.g. in this case compare strings by length
  {
    string[] test = ["ccc", "bb", "a", "eeeee", "dddd"];
    auto result1 = test.minElement!"a.length";
    assert(result1 == "a");
    auto result2 = test.maxElement!"a.length";
    assert(result2 == "eeeee");
  }

  // use temporary type that implements required ordering operations (opCmp)
  // e.g. in this case compare strings by length
  {
    struct Str {
      string str;
      int opCmp()(auto ref const Str rhs) const {
        if (str.length == rhs.str.length) return 0;
        return str.length < rhs.str.length ? -1 : +1;
      }
    }
    string[] test = ["ccc", "bb", "a", "eeeee", "dddd"];

    auto strs = test.map!(a => Str(a));

    auto result1 = strs.fold!(min, max);
    assert(result1[0].str == "a"); // min
    assert(result1[1].str == "eeeee"); // max

    auto result2 = strs.minElement;
    assert(result2.str == "a");

    auto result3 = strs.maxElement;
    assert(result3.str == "eeeee");
  }
}
