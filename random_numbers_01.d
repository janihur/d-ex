import std.random : uniform;

int[] randomIntArray(in int min, in int max, in int size)
@safe
in {
  assert(min > 0);
  assert(max > 0);
  assert(size > 0);
  assert(max > min);
} out(result) {
  assert(result.length == size);
  foreach (n; result) {
    assert(n >= min && n <= max);
  }
} do {
  int[] numbers;
  numbers.reserve(size);

  foreach (_; 0 .. size) numbers ~= uniform(min, max+1);

  return numbers;
}

int[] randomUniqueIntArray(in int min, in int max, in int size)
@safe
in {
  assert(min > 0);
  assert(max > 0);
  assert(size > 0);
  assert(max > min);
  assert((max - min + 1) >= size);
} out(result) {
  assert(result.length == size);
  foreach (n; result) {
    assert(n >= min);
    assert(n <= max);
  }
} do {
  int[] numbers;
  numbers.reserve(size);

  bool[int] exists;
  foreach(i; min .. max+1) exists[i] = false;
  
  int i = 0;
  do {
    auto number = uniform(min, max+1);
    if (!exists[number]) {
      numbers ~= number;
      exists[number] = true;
      ++i;
    }
  } while (i < size);
  
  return numbers;
}

int[] randomUniqueIntArrayV2(in int min, in int max, in int size)
@safe
in {
  assert(min > 0);
  assert(max > 0);
  assert(size > 0);
  assert(max > min);
  assert((max - min + 1) >= size);
} out(result) {
  assert(result.length == size);
  foreach (n; result) {
    assert(n >= min);
    assert(n <= max);
  }
} do {
  import std.algorithm.mutation : remove;
  
  int[] pool;
  pool.reserve(max - min + 1);
  foreach(i; min .. max+1) pool ~= i;

  int[] numbers;
  foreach(_; 0 .. size) {
    auto index = uniform(0, pool.length);
    numbers ~= pool[index];
    pool = pool.remove(index);
  }
  return numbers;
}

void main() {
  import std.algorithm : sort;
  import std.stdio : writeln;
  writeln(randomUniqueIntArrayV2(1, 40, 7).sort);
}
