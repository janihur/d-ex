import std.stdio;
import std.string;

void main() {
  writeln("-----");
  {
    string x = import("import_file_01.txt").chomp;
    assert(x == "This string is read compile time from import_file_01.txt !");
    writeln(typeof(x).stringof);
    writeln(x);
  }

  writeln("-----");
  {
    string[] x = import("import_file_02.txt").chomp.split("\n");
    assert(x[2] == "Line 3 from import_file_02.txt");
    writeln(typeof(x).stringof);
    writeln(x);
    writeln(typeof(x[2]).stringof);
    writeln(x[2]);
  }

  writeln("-----");
  {
    string[] x = mixin(import("import_file_03.txt"));
    assert(x[2] == "bar-3");
    writeln(typeof(x).stringof);
    writeln(x);
    writeln(typeof(x[2]).stringof);
    writeln(x[2]);
  }
}
