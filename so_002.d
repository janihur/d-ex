/**
Stack Overflow question: https://stackoverflow.com/q/53967917/272735

Why I can't use [`std.algorithm.iteration.sum`][sum] with `Duration[]` ?

I thought I can use the `sum` in the same way than e.g. with `int[]`:

    int[] ints = [40, 27, 5];
    int intSum = ints.sum();
    assert(intSum == 72);

But instead I get an unexpected (unexpected for me at least) compilation error:

    /usr/include/dmd/phobos/std/algorithm/iteration.d(5885): Error: struct `core.time.Duration` member this is not accessible
    so_002.d(11): Error: template instance `std.algorithm.iteration.sum!(Duration[])` error instantiating

I kind of understand the compilation error but I fail to understand why the limitation exists as for me there is nothing in documentation explaining this.

I have read:

* [`sum`][sum]
* [`Duration`][duration]

Doesn't `Duration[]` conform with the constraints of `sum`:

    auto sum(R)(R r)
    if (isInputRange!R && !isInfinite!R && is(typeof(r.front + r.front)));

Or have I overlooked something else ?

The workaround is easy - use [`fold`][fold] instead.

[sum]: https://dlang.org/phobos/std_algorithm_iteration.html#sum
[fold]: https://dlang.org/phobos/std_algorithm_iteration.html#fold
[duration]: https://dlang.org/phobos/core_time.html#.Duration
*/

import std.algorithm : fold, sum;
import std.datetime : Duration, minutes;

void main()
{
  Duration[] durations = [40.minutes, 27.minutes, 5.minutes];

  // Unexpected compilation error:
  // /usr/include/dmd/phobos/std/algorithm/iteration.d(5885): Error: struct `core.time.Duration` member this is not accessible
  // so_002.d(11): Error: template instance `std.algorithm.iteration.sum!(Duration[])` error instantiating

  // auto durationSum = durations.sum();

  // fold works as expected
  Duration durationSum = durations.fold!((a, b) => a + b);
  assert(durationSum == 72.minutes);
}
