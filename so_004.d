/*
Stack Overflow question: https://stackoverflow.com/q/56214209/272735

Extra null when a struct is printed with writeln

In the code below what is that extra `null` in the output when an instance of `S2` is printed with `writeln` ?

    $ dmd -de -w so_004.d && ./so_004
    S1("A", 1)
    S2("A", 1, null)

If I define `S2` in a package scope (i.e. outside `main` function) the `null` disappears.

Compiled with a reasonable recent DMD:

    $ dmd --version
    DMD64 D Compiler v2.083.0
    Copyright (C) 1999-2018 by The D Language Foundation, All Rights Reserved written by Walter Bright

I noticed the issue when I was learning `opEquals` and I'm not planning to define types in sub-scopes in "real" code.

*/

import std.stdio;

void main() {
  {
    struct S1 { string id; ushort x; }
    auto a = S1("A", 1);
    assert(a == a);
    writeln(a);
  }

  {
    struct S2 {
      string id; ushort x;
      bool opEquals()(auto ref const string rhs) const {
        return id == rhs;
      }
    }

    auto a = S2("A", 1);
    assert(a == "A");
    writeln(a);
  }
}
