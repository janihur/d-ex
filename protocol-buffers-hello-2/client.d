import std.algorithm.searching;
import std.array;
import std.datetime;
import std.stdio;

import google.protobuf;
import google.protobuf.timestamp;

import anymessage;
import echo;
import heartbeat;

enum BUFSIZE = 32768;

struct ServerConnection
{
  import std.socket;
  
  this(in string address, in ushort port)
  {
    this.address = address;
    this.port = port;
  }

  uint connect()
  {
    socket = new Socket(AddressFamily.INET, SocketType.STREAM);
    socket.connect(new InternetAddress(address, port));

    auto receivedBytes = socket.receive(buffer);
    auto input = buffer[0 .. receivedBytes];
    auto pong = input.fromProtobuf!Pong;
    
    debug writefln("DEBUG: (receivedBytes = %s)", receivedBytes);
    writefln("Server: (pong = %s)", pong);

    return pong.sequenceId;
  }

  ubyte[] receive()
  {
    auto receivedBytes = socket.receive(buffer);
    debug writefln("DEBUG: (receivedBytes = %s)(msg = %s)", receivedBytes, buffer[0 .. receivedBytes]);
    return buffer[0 .. receivedBytes];
  }

  void send(in ubyte[] msg)
  {
    auto sendBytes = socket.send(msg);
    debug writefln("DEBUG: (typeof(msg) = %s)(sendBytes = %s)", typeof(msg).stringof, sendBytes);
  }
  
  @disable this();
  @disable this(this);
  
private:
  string address;
  ushort port;
  Socket socket;
  ubyte[BUFSIZE] buffer;
}

ushort getopts(string[] args, ref ushort port)
{
  import std.getopt;
  
  try {
    auto help = getopt(
                       args,
                       std.getopt.config.required,
                       "port|p", "port the server will listen to", &port
                       );
  
    if (help.helpWanted) {
      defaultGetoptPrinter("Some information about the program.",
                           help.options);
      return 1;
    }
  } catch (GetOptException ex) {
    writeln(ex.msg);
    return 2;
  }

  return 0;
}

int main(string[] args)
{
  ushort port;
  {
    auto r = args.getopts(port);
    if (r > 0) return r;
  }

  auto server = ServerConnection("localhost", port);
  auto sequenceId = server.connect;

  // TODO: https://github.com/adamdruppe/arsd/blob/master/terminal.d

  foreach(line; stdin.byLine) {
    if (line.startsWith("quit")) {
       break;
    } else if (line.startsWith("echo")) {
      debug writeln("DEBUG: type: EchoRequest");
      AnyRequest any;
      EchoRequest echoRequest;
      echoRequest.message = line[5 .. $].idup;
      any.echo = echoRequest;
      server.send(any.toProtobuf.array);
    } else if (line.startsWith("ohce")) {
      debug writeln("DEBUG: type: EchoRequest");
      AnyRequest any;
      EchoRequest echoRequest;
      echoRequest.message = line[5 .. $].idup;
      echoRequest.reversed = true;
      any.echo = echoRequest;
      server.send(any.toProtobuf.array);
    } else if (line.startsWith("ping")) {
      debug writeln("DEBUG: type: Ping");
      AnyRequest any;
      Ping ping;
      ping.sequenceId = ++sequenceId;
      any.ping = ping;
      server.send(any.toProtobuf.array);
    } else {
      writeln("unknown command");
      continue;
    }

    auto raw = server.receive();
    auto anyResponse = raw.fromProtobuf!AnyResponse;
    debug writefln("DEBUG: (anyResponse = %s)", anyResponse);
    
    switch(anyResponse.anyResponseCase) {
    case AnyResponse.AnyResponseCase.echo:
      writefln("Server: (echo = %s)", anyResponse.echo);
      break;
    case AnyResponse.AnyResponseCase.pong:
      writefln("Server: (pong = %s)", anyResponse.pong);
      break;
    default:
      assert(0); // unknown message
    }
  }

  return 0;
}
