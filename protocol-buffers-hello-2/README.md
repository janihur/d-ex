D for Dummies by Example: Protocol Buffers and BSD Sockets
==========================================================

This example illustrates the following topics:

* [Protocol Buffers](https://developers.google.com/protocol-buffers/)
* Network client and server with BSD sockets. This is originally based on a recipe found from [Adam D. Ruppe](https://arsdnet.net/)'s [D Cookbook](https://www.packtpub.com/application-development/d-cookbook) and later adjusted a bit based on examples found from [std.socket](https://dlang.org/phobos/std_socket.html) documentation.
* Command line option processing with [std.getopt](https://dlang.org/phobos/std_getopt.html) standard library

The example has been compiled and tested only with [DMD64 compiler](https://dlang.org/dmd-linux.html) v2.083.0 in linux.

First [download and install](https://github.com/protocolbuffers/protobuf/releases) protocol buffer compiler _protoc_.

Second install D [protobuf](https://code.dlang.org/packages/protobuf) package and build protoc D codegen plugin:

```
dub fetch protobuf
cd ~/.dub/packages/protobuf-0.5.0/protobuf
dub build :protoc-gen-d
```

Now the protocol buffer compiler D codegen plugin is available in:

```
~/.dub/packages/protobuf-0.5.0/protobuf/build/protoc-gen-d
```

An example how to how to compile a proto-file on command line:

```
mkdir generated
protoc \
 --plugin=$HOME/.dub/packages/protobuf-0.5.0/protobuf/build/protoc-gen-d \
 --d_opt=message-as-struct \
 --d_out=generated \
 --proto_path . \
 hello.proto
```

The D and proto code is compiled with _make_:

```
make
```

Start server:

```
./server -p 2525
```

Start client:

```
./client -p 2525
```

The client goes into interactive mode where it supports the following commands: _ping_, _echo_, _ohce_ and _quit_. Example:

```
$ ./client -p 2525
DEBUG: (receivedBytes = 18)
Server: (pong = Pong(62814, 2019-Mar-30 08:41:13.9937891+00:00))
ping
DEBUG: type: Ping
DEBUG: (typeof(msg) = const(ubyte[]))(sendBytes = 6)
DEBUG: (receivedBytes = 19)(msg = [18, 17, 8, 223, 234, 3, 18, 11, 8, 179, 215, 252, 228, 5, 16, 212, 255, 226, 114])
DEBUG: (anyResponse = AnyResponse(pong, #{overlap _echo, _pong}))
Server: (pong = Pong(62815, 2019-Mar-30 08:41:23.2406973+00:00))
echo föôbár
DEBUG: type: EchoRequest
DEBUG: (typeof(msg) = const(ubyte[]))(sendBytes = 13)
DEBUG: (receivedBytes = 13)(msg = [10, 11, 10, 9, 102, 195, 182, 195, 180, 98, 195, 161, 114])
DEBUG: (anyResponse = AnyResponse(echo, #{overlap _echo, _pong}))
Server: (echo = EchoResponse("föôbár"))
ohce föôbár
DEBUG: type: EchoRequest
DEBUG: (typeof(msg) = const(ubyte[]))(sendBytes = 15)
DEBUG: (receivedBytes = 13)(msg = [10, 11, 10, 9, 114, 195, 161, 98, 195, 180, 195, 182, 102])
DEBUG: (anyResponse = AnyResponse(echo, #{overlap _echo, _pong}))
Server: (echo = EchoResponse("rábôöf"))
quit
$
```

You can observe the network traffic e.g. with [tcpflow](https://github.com/simsong/tcpflow):

```
# tcpflow -c -i lo port 2525
```

Note you have to be root to have enough permissions.
