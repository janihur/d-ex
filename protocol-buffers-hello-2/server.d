import std.array;
import std.datetime;
import std.random;
import std.socket;
import std.stdio;

import google.protobuf;
import google.protobuf.timestamp;

import anymessage;
import echo;
import heartbeat;

enum BUFSIZE = 32768;
enum BACKLOG = 10;

ushort getopts(string[] args, ref ushort port)
{
  import std.getopt;

  try {
    auto help = getopt(
                       args,
                       std.getopt.config.required,
                       "port|p", "port the server will listen to", &port
                       );
  
    if (help.helpWanted) {
      defaultGetoptPrinter("Some information about the program.",
                           help.options);
      return 1;
    }
  } catch (GetOptException ex) {
    writeln(ex.msg);
    return 2;
  }

  return 0;
}

int main(string[] args)
{
  debug writefln("DEBUG: (args = %s)", args);

  ushort port;
  {
    auto r = args.getopts(port);
    if (r > 0) return r;
  }

  debug writefln("DEBUG: (port = %d)", port);
  
  auto listener = new Socket(AddressFamily.INET, SocketType.STREAM);
  listener.bind(new InternetAddress("localhost", port));
  listener.listen(BACKLOG);

  auto readSet = new SocketSet();
  Socket[] connectedClients;
  ubyte[BUFSIZE] buffer;
  bool isRunning = true;

  while (isRunning) {
    readSet.reset();
    readSet.add(listener);
    
    debug {
      foreach(i, client; connectedClients) {
        if (client.isAlive) {
          writefln("DEBUG: client: (i = %s)(isAlive = %s)(remoteAddress = %s)",
                   i, client.isAlive, client.remoteAddress);
        } else {
          writefln("DEBUG: client: (i = %s)(isAlive = %s)",
                   i, client.isAlive);
        }
      }
    }

    // remove dead client sockets before adding them to SocketSet
    import std.algorithm : filter;
    connectedClients = connectedClients
      .filter!(client => client.isAlive)
      .array;

    foreach(client; connectedClients) readSet.add(client);

    if (Socket.select(readSet, null, null)) {

      // existing clients
      foreach(client; connectedClients) {
        if (readSet.isSet(client)) {
          auto receivedBytes = client.receive(buffer);
          
          if (receivedBytes > 0 ) {
            auto input = buffer[0 .. receivedBytes];
            auto anyRequest = input.fromProtobuf!AnyRequest;
            debug writefln("DEBUG: (remoteAddress = %s)(receivedBytes = %s)(anyRequest = %s)", client.remoteAddress, receivedBytes, anyRequest);

            // process messages
            switch(anyRequest.anyRequestCase) {
            case AnyRequest.AnyRequestCase.echo:
              debug writeln("DEBUG: received EchoRequest message");
              auto echoRequest = anyRequest.echo;
              debug writefln("DEBUG: (echoRequest = %s)", echoRequest);

              AnyResponse response;
              EchoResponse echo;
              // reverse message if requested
              if (echoRequest.reversed) {
                import std.range : retro;
                import std.conv : text;
                echo.message = echoRequest.message.retro.text;
              } else {
                echo.message = echoRequest.message;
              }
              response.echo = echo;

              client.send(response.toProtobuf.array);
              break;
            case AnyRequest.AnyRequestCase.ping:
              debug writeln("DEBUG: received Ping (request) message");
              auto ping = anyRequest.ping;
              debug writefln("DEBUG: (ping = %s)", ping);

              AnyResponse response;
              Pong pong;
              pong.sequenceId = ping.sequenceId;
              pong.time = Timestamp(Clock.currTime());

              response.pong = pong;

              client.send(response.toProtobuf.array);
              break;
            default:
              assert(0); // unknown message
            }
          } else {
            if (receivedBytes == Socket.ERROR) {
              debug writefln("DEBUG: connection failure: (receivedBytes = %s)", receivedBytes);
            } else {
              debug writefln("DEBUG: client has closed the connection: (receivedBytes = %s)", receivedBytes);
            }
            client.close;
          }
        }
      }

      // new client connection
      if (readSet.isSet(listener)) {
        auto newSocket = listener.accept();

        Pong pong;
        pong.sequenceId = uniform(ushort.min, ushort.max);
        pong.time = Timestamp(Clock.currTime());
        
        debug writefln("DEBUG: (pong = %s)", pong);
        newSocket.send(pong.toProtobuf.array);
        connectedClients ~= newSocket;
      }
    }
  }

  return 0;
}
