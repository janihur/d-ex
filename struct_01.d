import std.algorithm;
import std.stdio;

void main ()
{
  // int
  {
    auto list = [1, 2, 3, 4];
    auto x = list.find(2);

    assert(x.length == 3);
    assert(x[0] == 2);

    // writeln(x);
  }
  
  // string
  {
    auto list = ["A", "B", "C", "D"];
    auto x = list.find("B");

    assert(x.length == 3);
    assert(x[0] == "B");

    // writeln(x);
  }
  
  // struct w/ default opEquals
  {
    struct S1 { string id; }
    auto list = [S1("A"), S1("B"), S1("C"), S1("D")];
    auto x = list.find(S1("B"));

    assert(x.length == 3);
    assert(x[0] == S1("B"));

    // writeln(x);
  }

  // struct w/ default opEquals + find predicate
  {
    struct S2 { string id; ushort x; }
    auto list = [S2("A", 0), S2("B", 2), S2("C", 3), S2("D", 4)];
    auto x = list.find!((a, b) => a.id == b.id)(S2("B", 0));

    assert(x.length == 3);
    assert(x[0] == S2("B", 2));
    
    writeln(x);
  }

  // struct w/ user defined opEquals
  {
    struct S3 {
      string id; ushort x;
      bool opEquals()(auto ref const S3 rhs) const {
        return id == rhs.id && x == rhs.x;
      }
      bool opEquals()(auto ref const string rhs) const {
        return id == rhs;
      }
    }

    assert(S3("A", 0) == "A");
    assert(S3("A", 0) != "B");
    assert(S3("A", 0) == S3("A", 0));
    assert(S3("A", 0) != S3("A", 1));
    
    auto list = [S3("A", 0), S3("B", 2), S3("C", 3), S3("D", 4)];

    auto x = list.find("B");

    assert(x[0] == "B");
    assert(x[0] == S3("B", 2));

    writeln(x);
  }
}
