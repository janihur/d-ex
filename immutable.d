import std.stdio : writefln;

void main()
{
  // how to remove immutability in variable declaration
  // https://stackoverflow.com/q/55176071/272735
  {
    immutable uint immutableX = 42;
    pragma(msg, "immutableX: ", typeof(immutableX));
    static assert(is(typeof(immutableX) == immutable(uint)));

    auto mutableX = cast()immutableX;
    pragma(msg, "  mutableX: ", typeof(mutableX));
    static assert(is(typeof(mutableX) == uint));

    mutableX *= 2;

    writefln("(immutableX = %d)(mutableX = %d)", immutableX, mutableX);
  }
}
