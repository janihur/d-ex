D := dmd -de -w
UNITTEST := -unittest -cov
RUN := -main -run

.PHONY: all
all: associative_arrays cr_001 datetime_01 dump_variable fizzbuzz immutable import_file random_numbers_01 random_numbers_02 range_01 rectangle_intersection slice_01 struct_01 so_001 so_002 so_003 so_004 so_005 so_006

associative_arrays: associative_arrays.d
	$(D) $(USER_OPTIONS) $<
	./$@

cr_001: cr_001.d
	$(D) $(UNITTEST) $(USER_OPTIONS) $(RUN) $<
	@tail -1 $@.lst

datetime_01: datetime_01.d
	$(D) $(USER_OPTIONS) $<
	./$@

dump_variable: dump_variable.d
	$(D) -debug $<
	./$@

fizzbuzz: fizzbuzz.d
	$(D) $(UNITTEST) $(USER_OPTIONS) $(RUN) $<
	@tail -1 $@.lst

immutable: immutable.d
	$(D) $<
	./$@

import_file: import_file.d import_file_0?.txt
	$(D) -J. $<
	./$@

random_numbers_01: random_numbers_01.d
	$(D) $(UNITTEST) $<

random_numbers_02: random_numbers_02.d
	$(D) $(UNITTEST) $<

range_01: range_01.d
	$(D) $(UNITTEST) $(USER_OPTIONS) $(RUN) $<
	@tail -1 $@.lst

rectangle_intersection: rectangle_intersection.d
	$(D) $(UNITTEST) $(USER_OPTIONS) $(RUN) $<
	@tail -1 $@.lst

so_001: so_001.d
	dmd $(UNITTEST) -debug $(RUN) $<
	@tail -1 $@.lst

struct_01: struct_01.d
	$(D) $<
	./$@

so_002: so_002.d
	$(D) $<
	./$@

so_003: so_003.d
	$(D) $<
	./$@

so_004: so_004.d
	$(D) $<
	./$@

so_005: so_005.d
	$(D) $<
	./$@

so_006: so_006.d
	$(D) -debug $<
	./$@

slice_01: slice_01.d
	$(D) $<
	./$@

.PHONY: clean
clean:
	@rm -f *~ *.o *.lst \
	associative_arrays \
	cr_001 \
	datetime_01 \
	dump_variable \
	fizzbuzz \
	immutable \
	import_file \
	random_numbers_0? \
	range_0? \
	rectangle_intersection \
	slice_01 \
	struct_0? \
	so_00?
