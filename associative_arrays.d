void print(T)(T value)
{
  import std.stdio : writeln;

  writeln("------");
  writeln(" Type: ", typeof(value).stringof);
  writeln("Value: ", value);
}

void main()
{
  {
    // note two elements with the same max value
    auto x = [1:100, 2:200, 3:999, 4:400, 5:500, 6:999];
    print(x);

    {
      import std.array : byPair;
      auto x1 = x.byPair;
      print(x1);

      import std.algorithm : maxElement;
      // only returns one of the max values
      auto x2 = x1.maxElement!(a => a.value);
      print(x2);

      auto x3 = x2.key;
      print(x3);
      assert(x3 == 3);
    }

    {
      auto x1 = x.byKeyValue;
      print(x1);

      import std.algorithm : maxElement;
      // only returns one of the max values
      auto x2 = x1.maxElement!(a => a.value);
      print(x2);

      auto x3 = x2.key;
      print(x3);
      assert(x3 == 3);
    }
  }
}
