// arsd Terminal Example 1
//
// Demonstrates
//
// * a terminal of type ConsoleOutputType.linear
// * a real time keyboard input with RealTimeConsoleInput
// * how to write a colorful text

// arsd.terminal copied from:
// https://github.com/adamdruppe/arsd/blob/master/terminal.d
import arsd.terminal;

// a simple state that is manipulated with keys
struct entity {
  string id;
  bool state;
  uint counter;

  void toggle() {
    state = !state;
  }

  void increment() {
    counter += 1;
  }
}

// from command line arguments to program start state
entity[] makeEntities(string[] args) {
  import std.algorithm : map;
  import std.array : array;
  return args.map!(a => entity(a, false, 0)).array;
}

// helper to write a colorful text
void green(T)(Terminal* t, T str) {
  t.color(Color.green, Color.DEFAULT);
  t.writeln(str);
  t.reset;
}

// helper to write a colorful text
void red(T)(Terminal* t, string fmt, T str) {
  t.color(Color.red, Color.DEFAULT);
  t.writefln(fmt, str);
  t.reset;
}

void main(string[] args) {
  auto entities = makeEntities(args[1..$]);
  
  auto term = Terminal(ConsoleOutputType.linear);
  term.clear;

  term.color(Color.green, Color.black);
  term.writeln("Hello world, in green on black!");
  term.reset;
  term.writeln("And back to normal.");

  term.writefln("(entities = %s)", entities);
  term.writeln("Now press some keys to manipulate entities' state. Known keys are: F1, F2, F3, F4 and End.");
  // term.writeln;
  
  term.hideCursor;
  
  auto input = RealTimeConsoleInput(&term, ConsoleInputFlags.raw);

  auto exit = false;

  do {
    dchar ch;
    try {
      ch = input.getch();
    } catch (UserInterruptionException) {
      ch = KeyboardEvent.Key.End;
    }
    
    switch(ch) {
      case KeyboardEvent.Key.F1:
        green(&term, "Key F1 pressed.");
        if (entities.length > 0) {
          entities[0].toggle;
          entities[0].increment;
        }
        break;
      case KeyboardEvent.Key.F2:
        green(&term, "Key F2 pressed.");
        if (entities.length > 1) {
          entities[1].toggle;
          entities[1].increment;
        }
        break;
      case KeyboardEvent.Key.F3:
        green(&term, "Key F3 pressed.");
        if (entities.length > 2) {
          entities[2].toggle;
          entities[2].increment;
        }
        break;
      case KeyboardEvent.Key.F4:
        green(&term, "Key F4 pressed.");
        if (entities.length > 3) {
          entities[3].toggle;
          entities[3].increment;
        }
        break;
      case KeyboardEvent.Key.End:
        exit = true;
        break;
      default:
        red(&term, "Unknown key (%s) pressed !", ch);
        break;
    }
  } while (!exit);
  
  term.writefln("(entities = %s)", entities);
}
