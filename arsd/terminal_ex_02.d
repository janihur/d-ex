// arsd Terminal Example 2
//
// Demonstrates
//
// * a terminal of type ConsoleOutputType.cellular
// * a real time keyboard input with RealTimeConsoleInput
// * how to position cursor to the screen
// * how to clear line (clearLine)

// arsd.terminal copied from:
// https://github.com/adamdruppe/arsd/blob/master/terminal.d
import arsd.terminal;

// clear a line by overwritting it with spaces
void clearLine(Terminal* term) {
  import std.string : leftJustify;
  term.write(leftJustify("", term.width - term.cursorX, ' '));
  term.moveTo(0, term.cursorY);
}

// prompt and wait until a key pressed (the key is discarded)
void pause(Terminal* term, RealTimeConsoleInput* input) {
  term.write("Press any key to continue");
  input.getch;
  term.moveTo(0, term.cursorY);
  clearLine(term);
}

// cursor position
struct Pos {
  uint x;
  uint y;

  string toString() const {
    import std.format : format;
    return format("(%s, %s)", x, y);
  }
}

Pos getPos(Terminal* term) {
  return Pos(term.cursorX, term.cursorY);
}

void setPos(Pos pos, Terminal* term) {
  term.moveTo(pos.x, pos.y);
}

void displayCursorPosition(Terminal* term) {
  auto pos = getPos(term);
  term.moveTo(0, 1);
  clearLine(term);
  term.writef("cursor position: %s", pos);
  pos.setPos(term);
}

void main() {
  auto term = Terminal(ConsoleOutputType.cellular);
  auto input = RealTimeConsoleInput(&term, ConsoleInputFlags.raw);

  term.hideCursor;
  
  term.writefln("terminal size: (%s, %s)", term.width, term.height);
  displayCursorPosition(&term);
  term.writeln;
  
  term.writeln("Meaningless mambo-jambo that will be overwritten shortly.");
  
  displayCursorPosition(&term);
  pause(&term, &input);

  term.moveTo(0, 2);
  clearLine(&term);
  term.writeln("Important message !");

  displayCursorPosition(&term);
  pause(&term, &input);

  {
    term.writeln("Wrote a character to the bottom right corner.");
    // save current cursor position so that we can return there
    auto pos = getPos(&term);

    term.moveTo(term.width - 2, term.height - 2);
    term.write("X");

    displayCursorPosition(&term);

    pos.setPos(&term);

    pause(&term, &input);
  }

  term.writeln("Time to say goodbye !");
  displayCursorPosition(&term);
  pause(&term, &input);
}
