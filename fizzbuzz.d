struct FizzBuzz {
  import std.conv : to;
  
  uint fizzNumber;
  uint buzzNumber;

  private uint number = 1;

  enum empty = false;

  string front()
    const @property
  {
    if (number % (fizzNumber * buzzNumber) == 0) return "Fizz Buzz";
    if (number % buzzNumber == 0) return "Buzz";
    if (number % fizzNumber == 0) return "Fizz";
    return to!(string)(number);
  }

  void popFront()
  {
    number++;
  }
} unittest {
  import std.array : array;
  import std.range : drop, take;
  
  string[] expected36 = ["1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "Fizz Buzz", "16", "17", "Fizz", "19", "Buzz", "Fizz", "22", "23", "Fizz", "Buzz", "26", "Fizz", "28", "29", "Fizz Buzz", "31", "32", "Fizz", "34", "Buzz", "Fizz"];

  FizzBuzz fb = FizzBuzz(3, 5);

  assert(fb.take(6).array == expected36[0 .. 6]);
  assert(fb.take(10).drop(6).array == expected36[6 .. 10]);
  assert(fb.take(36).array == expected36);

  debug {
    import std.stdio: writefln;
    auto x = fb.take(6).array;
    foreach(i, n; x) writefln("(%d): %s", i + 1, n);
  }
}
