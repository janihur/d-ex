import std.random;
import std.stdio;

void main()
{
  uint x = uniform(ushort.min, ushort.max);
  writefln("(ushort.min = %s)(ushort.max = %s)", ushort.min, ushort.max);
  writefln("(uint.min = %s)(uint.max = %s)", uint.min, uint.max);
  writefln("(x = %s)", x);
}
