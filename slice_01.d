void printMeta(in string title, in int[] a)
{
  import std.stdio : writefln;
  writefln("%s: (ptr = %s)(length = %d)(capacity = %d)", title, a.ptr, a.length, a.capacity);
}

void append(ref int[] a, in int howMany) {
  import std.range : iota;
  foreach (i; iota(1, howMany + 1)) a ~= i;
}

void main()
{
  {
    int[] a; // no allocation for the behind-the-scenes array
    assert(a.length == 0);
    assert(a.capacity == 0);
    
    printMeta("#1.1", a);
    
    a.append(5); // allocates
    assert(a.length == 5);
    assert(a.capacity >= 5);

    assert(a[0] == 1);
    assert(a[4] == 5);
    
    printMeta("#1.2", a);
  }

  {
    int[] a = new int[](10); // allocates and initializes 10 ints
    assert(a.length == 10);
    assert(a.capacity >= 10);

    assert(a[0] == 0);
    assert(a[9] == 0);

    printMeta("#2.1", a);
    
    a.append(6); // reallocates
    assert(a.length == 16);
    assert(a.capacity >= 16);

    assert(a[10] == 1);
    assert(a[11] == 2);

    printMeta("#2.2", a);
  }

  {
    int[] a; // no allocation for the behind-the-scenes array
    a.length = 10; // allocates and initializes 10 ints
    assert(a.length == 10);
    assert(a.capacity >= 10);

    assert(a[0] == 0);
    assert(a[9] == 0);

    printMeta("#3.1", a);

    a.append(6); // reallocates
    assert(a.length == 16);
    assert(a.capacity >= 16);

    assert(a[10] == 1);
    assert(a[11] == 2);

    printMeta("#3.2", a);
  }

  {
    int[] a; // no allocation for the behind-the-scenes array
    a.reserve(10); // allocates but the memory is not initialized

    assert(a.length == 0);
    assert(a.capacity >= 10);

    printMeta("#4.1", a);

    a.append(5); // no allocation
    assert(a.length == 5);
    assert(a.capacity >= 5);
    
    assert(a[0] == 1);
    assert(a[4] == 5);

    printMeta("#4.2", a);
  }
}
