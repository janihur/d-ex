void dump(alias x)()
{
  import std.stdio : writefln;
  writefln("(%s %s = %s)", typeid(typeof(x)), x.stringof, x);
}

// original code copied from https://forum.dlang.org/
// void dump(alias variable)()
// {
//   writeln("Dumping ",typeid(typeof(variable)),":\n");
//   writeln(variable.stringof, " = ", variable);
//   foreach(member; FieldNameTuple!(typeof(variable)))
//     {
//       writeln("\t", member, ": ", mixin("variable."~member) );
//     }
//   writeln("}\n");
// }

void main()
{
  {
    auto x = 5;
    debug dump!x;
  }
  {
    auto lorem = "Lorem ipsum sit dolor sit amet"d;
    debug dump!lorem;
  }
  {
    import std.datetime;
    auto durations = [1.minutes, 10.minutes, 100.minutes];
    debug dump!durations;
  }
  {
    auto f = (int x) => x + 1;
    debug dump!f;
  }
  {
    int delegate(int) d = x => x + 1;
    debug dump!d;
  }
  {
    // BEWARE a compiler bug: https://issues.dlang.org/show_bug.cgi?id=13617
    // "function-local symbols do not have unique names and conflict with
    // symbols in sibling scopes"

    auto x = "STRING EXPECTED";
    debug dump!x; // output: (int x = 5)
  }
}
