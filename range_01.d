// How to
// 1) return a range and
// 2) write a function that accepts that range.

import std.range.primitives : ElementType, isInputRange, walkLength;
import std.stdio : writefln;

struct Foo {
  uint id;
  State state;
  uint counter;
}

enum State { PASSIVE, LOW, HIGH, ACTIVE }

auto getActives(Foo[] foos) {
  import std.algorithm.iteration : filter;
  return foos.filter!(a => a.state > State.LOW);
}

R increaseCounter(R)(R foos, uint increment)
if (isInputRange!R && is(ElementType!R == Foo))
{
  foreach (ref foo; foos) {
    foo.counter += increment;
  }

  return foos;
}

unittest {
  Foo[] foos = [
    Foo(1, State.LOW,     0),
    Foo(2, State.PASSIVE, 0),
    Foo(3, State.ACTIVE,  0),
    Foo(4, State.HIGH,    0),
  ];

  writefln("Original: %s", foos);

  foos.getActives.increaseCounter(3).increaseCounter(3);

  assert(foos[0].counter == 0);
  assert(foos[1].counter == 0);
  assert(foos[2].counter == 6);
  assert(foos[3].counter == 6);

  writefln("Modified: %s", foos);
}
