// Category Theory for Programmers - Challenges Chapter 2
//
// 1. Define a higher-order function (or a function object) memoize in your
//    favorite language. This function takes a pure function f as an argument
//    and returns a function that behaves almost exactly like f, except that
//    it only calls the original function once for every argument, stores the
//    result internally, and subsequently returns this stored result every
//    time it’s called with the same argument. You can tell the memoized
//    function from the original by watching its performance. For instance,
//    try to memoize a function that takes a long time to evaluate. You’ll
//    have to wait for the result the first time you call it, but on
//    subsequent calls, with the same argument, you should get the result
//    immediately.
// 2. Try to memoize a function from your standard library that you normally
//    use to produce random numbers. Does it work?
// 3. Most random number generators can be initialized with a seed. Implement
//    a function that takes a seed, calls the random number generator with
//    that seed, and returns the result. Memoize that function. Does it work?
// 5. How many different functions are there from Bool to Bool? Can you
//    implement them all?

import std.stdio : writef, writefln, writeln;

void dump(alias variable)()
{
  import std.stdio : writefln;
  writefln("(%s %s = %s)", typeid(typeof(variable)), variable.stringof, variable);
}

// recursive implementation that is ineffective in DMD
// notable delay (in my computer) when n > 40
int fibonacci(int n)
  pure
in {
  assert (n < 94);
} do {
  return (n < 2) ? n : fibonacci(n - 1) + fibonacci(n - 2);
}

// adapted from:
// http://dpldocs.info/experimental-docs/std.datetime.stopwatch.benchmark.html
import core.time : Duration;
Duration benchmark(int delegate(int) dg, int x)
{
  import std.datetime.stopwatch : AutoStart, StopWatch;
  auto sw = StopWatch(AutoStart.yes);
  dg(x);
  sw.stop();
  return sw.peek;
}

// TODO template for different types ?
// TODO cache as interface instead of hard coded type ?
int delegate(int) memoizeV1(ref int[int] cache, int delegate(int) pure d)
  pure
{
  return delegate (int x) {
    if (int* _ = x in cache) {
      debug writef("memoizeV1: cache hit for (x = %s)(r = %s)", x, cache[x]);
      return cache[x];
    } else {
      debug writef("memoizeV1: calculating result for (x = %s)", x);
      auto result = d(x);
      cache[x] = result;
      return result;
    }
  };
}

void challenge1()
{
  int[int] cache;
  import std.functional : toDelegate;
  auto m1 = cache.memoizeV1(toDelegate(&fibonacci));
  debug dump!m1;

  debug writeln("feeding the cache:");
  foreach (i; [10, 20, 30, 40, 45, 46]) {
    auto dur = benchmark(m1, i);
    debug writefln(" took %s msecs", dur.total!"msecs");
  }
  
  debug writeln("collecting results from the cache:");
  foreach (i; [10, 20, 30, 40, 45, 46]) {
    auto dur = benchmark(m1, i);
    debug writefln(" took %s usecs", dur.total!"usecs");
  }
  debug dump!cache;
}

// http://dpldocs.info/experimental-docs/std.functional.memoize.2.html
void challenge1b()
{
  import std.datetime.stopwatch : AutoStart, StopWatch;
  import std.functional : memoize;

  debug writeln("feeding the cache:");
  foreach (i; [10, 20, 30, 40, 45, 46]) {
    auto sw = StopWatch(AutoStart.yes);
    memoize!fibonacci(i);
    sw.stop();
    debug writefln("with std.functional.memoize took %s msecs",
                   sw.peek.total!"msecs");
  }
  
  debug writeln("collecting results from the cache:");
  foreach (i; [10, 20, 30, 40, 45, 46]) {
    auto sw = StopWatch(AutoStart.yes);
    memoize!fibonacci(i);
    sw.stop();
    debug writefln("with std.functional.memoize took %s usecs",
                   sw.peek.total!"usecs");
  }
  
}

void challenge2and3()
{
  import std.functional : memoize;
  import std.random : MinstdRand0, uniform;
  import std.range : array, take;

  {
    // uniform initializes with unpredictable seed
    ushort x1 = uniform!"(]"(ushort.min, ushort.max);
    dump!x1;
  }

  // /usr/include/dmd/phobos/std/random.d(1612): Error: static variable initialized cannot be read at compile time
  // /usr/include/dmd/phobos/std/random.d(1655):        called from here: rndGen()
  // /usr/include/dmd/phobos/std/random.d(1655):        called from here: uniform(a, b, rndGen())
  // memoize!(uniform!"(]"(ushort.min, ushort.max));

  {
    // predictable seed returns always the same sequence
    // but rnd0 is not a function but a range that can't be memoized (afaik)
    auto rnd0 = MinstdRand0(1);
    writefln("(rnd0 = %s)", typeof(rnd0).stringof);
    auto x2 = rnd0.front;
    dump!x2;
    assert(x2 == 16807);
    // the 1st item is the same than x2 because no popFront yet
    uint[] x3 = rnd0.take(10).array;
    dump!x3;
    assert(x3[0] == 16807);
    assert(x3[9] == 2007237709);
  }
}

// boolean functions
bool bool1(bool b) pure { return b; }
bool bool2(bool b) pure { return !b; }
bool bool3(bool b) pure { return true; }
bool bool4(bool b) pure { return false; }

void challenge5()
{
  assert(bool1(true)  == true);
  assert(bool1(false) == false);

  assert(bool2(true)  == false);
  assert(bool2(false) == true);

  assert(bool3(true)  == true);
  assert(bool3(false) == true);

  assert(bool4(true)  == false);
  assert(bool4(false) == false);

  writeln("all assertions ok");
}

void main()
{
  writeln("--- memoize (own implementation)");
  challenge1;

  writeln("--- memoize (std.functional.memoize)");
  challenge1b;

  writeln("--- memoize random number generator");
  // challenges 2 and 3 are essentially impossible (afaik)
  challenge2and3;

  writeln("--- boolean to boolean functions");
  challenge5;
}
