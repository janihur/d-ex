// Category Theory for Programmers - Challenges Chapter 1
//
// 1. Implement, as best as you can, the identity function in your favorite
//    language (or the second favorite, if your favorite language happens to
//    be Haskell).
// 2. Implement the composition function in your favorite language. It takes
//    two functions as arguments and returns a function that is their
//    composition.
// 3. Write a program that tries to test that your composition function
//    respects identity.

import std.stdio : writefln, writeln;
import std.traits : isDelegate, isFunction, isFunctionPointer;

// https://dlang.org/spec/function.html#return-ref-parameters
// ref int identity(return ref int x) {
//   return x; // pass-through function that does nothing
// }

T identity(T)(T t) pure {
  return t;
}

int delegate(int) composeV1(int delegate(int) d1, int delegate(int) d2) pure {
  return x => d2(d1(x));
}

T delegate(T) composeV2(T)(T delegate(T) d1, T delegate(T) d2) pure {
  return x => d2(d1(x));
}

void dump(alias variable)()
{
  import std.stdio : writefln;
  writefln("(%s %s = %s)", typeid(typeof(variable)), variable.stringof, variable);
}

void main()
{
  writeln("--- identity function");
  {
    // int ---------------------------------------------------------------------
    
    auto int1 = identity(5);
    debug dump!int1;
    assert(int1 == 5);
    assert(is(typeof(int1) == int));
    assert(typeid(typeof(int1)) == typeid(typeof(5)));

    auto int2 = identity(int1);
    debug dump!int2;
    assert(int2 == 5);
    assert(typeid(typeof(int2)) == typeid(typeof(int1)));

    // string ------------------------------------------------------------------
    
    auto string1 = identity("foo");
    debug dump!string1;
    assert(string1 == "foo");
    assert(is(typeof(string1) == string));
    assert(typeid(typeof(string1)) == typeid(typeof("foo")));

    auto string2 = identity(string1);
    debug dump!string2;
    assert(string2 == "foo");
    assert(typeid(typeof(string2)) == typeid(typeof(string1)));

    // struct ------------------------------------------------------------------
    
    struct S { int s; }

    auto struct1 = identity(S(100));
    debug dump!struct1;
    assert(struct1 == S(100));
    assert(is(typeof(struct1) == struct));
    assert(typeid(typeof(struct1)) == typeid(typeof(S(100))));

    auto struct2 = identity(struct1);
    debug dump!struct2;
    assert(struct2 == struct1);
    assert(typeid(typeof(struct2)) == typeid(typeof(struct1)));

    // delegate ----------------------------------------------------------------
    
    int d (int x) { return 0; }
    
    auto delegate1 = identity(&d);
    debug dump!delegate1;
    assert(delegate1 == &d);
    assert(is(typeof(delegate1) == delegate));
    assert(isDelegate!delegate1);
    assert(typeid(typeof(delegate1)) == typeid(typeof(&d)));

    auto delegate2 = identity(delegate1);
    debug dump!delegate2;
    assert(delegate2 == delegate1);
    assert(typeid(typeof(delegate2)) == typeid(typeof(delegate1)));

    // function ----------------------------------------------------------------

    static assert(isFunction!(identity!int));

    auto function1 = identity(&identity!int);
    debug dump!function1;
    assert(function1 == &identity!int);
    static assert(isFunctionPointer!function1);
    assert(typeid(typeof(function1)) == typeid(typeof(&identity!int)));

    auto function2 = identity(function1);
    debug dump!function2;
    assert(function2 == function1);
    assert(typeid(typeof(function2)) == typeid(typeof(function1)));
  }

  writeln("--- std.functional : compose, pipe");
  {
    // https://dlang.org/phobos/std_functional.html
    import std.functional : compose, pipe;

    string appendA(string str) pure { return str ~= "A"; }
    string appendB(string str) pure { return str ~= "B"; }
    
    // runs left to right: appendA -> appendB
    auto x1 = "".pipe!(appendA, appendB);
    debug dump!x1;
    assert(x1 == "AB");

    // runs right to left: appendA <- appendB
    auto x2 = "".compose!(appendA, appendB);
    debug dump!x2;
    assert(x2 == "BA");
  }

  writeln("--- delegates: http://ddili.org/ders/d.en/lambda.html");
  {
    int delegate(int) makeAdderV1(int increment) pure {
      return
        // all the following are equal:
        // delegate (int x) { return increment + x; };
        // (int x) { return increment + x; };
        // (int x) => increment + x;
        // (x) => increment + x;
        x => increment + x;
    }

    auto adder1 = makeAdderV1(10);
    debug dump!adder1;
    assert(13 == adder1(3));

    int delegate(int) makeAdder2(int delegate(int) d) pure {
      return d;
    }

    auto y = 10;
    auto adder2 = makeAdder2(x => y + x);
    debug dump!adder2;
    assert(13 == adder2(3));

    alias AdderType = int delegate(int);

    // compose for two AdderTypes
    AdderType makeAdder3(AdderType d1, AdderType d2) pure {
      return x => d2(d1(x));
    }
    auto adder3 = makeAdder3(makeAdderV1(3), x => x + y);
    debug dump!adder3;
    assert(16 == adder3(3));

    // trying to use std.functional.pipe (or compose) with adders above
    // results the following errors:
    //
    // Error: variable adder3 cannot be read at compile time
    // pipe!(adder3(3))(0);
    //
    // Error: closures are not yet supported in CTFE
    // https://issues.dlang.org/show_bug.cgi?id=6958
    // pipe!(makeAdderV1(3))(0);
  }

  writeln("--- composition function");
  {
    immutable x = 1;
    immutable y = 3;

    int delegate(int) d1 () pure { return x_ => x_ + y; }

    // 1st param: lambda adds value of x (1)
    // 2nd param: delegade d1 adds value of y (3)
    auto add4 = composeV1(x_ => x_ + x, d1);
    debug dump!add4;

    assert(is(typeof(add4) == delegate));
    assert(isDelegate!add4);

    auto nine = add4(5);
    debug dump!nine;
    assert(9 == nine);

    auto addX = composeV2((int x_) => x_ + x, d1);
    // this syntax works too
    // auto addX = composeV2!(int)(x_ => x_ + x, d1);
    debug dump!addX;
    
    assert(is(typeof(addX) == delegate));
    assert(isDelegate!addX);

    auto eleven = addX(7);
    debug dump!eleven;
    assert(11 == eleven);

    // this syntax works too
    // auto addS =
    //   composeV2!(string)(composeV2!(string)(x_ => x_ ~ "FOO",
    //                                         x_ => x_ ~ "BAR"),
    //                      x_ => x_ ~ "ZOO");
    auto addS =
      composeV2((string x_) => x_ ~ "FOO",
                (string x_) => x_ ~ "BAR").
      composeV2((string x_) => x_ ~ "ZOO");
    debug dump!addS;
    
    assert(is(typeof(addS) == delegate));
    assert(isDelegate!addS);

    auto str = addS("Composed string: ");
    debug dump!str;
    assert("Composed string: FOOBARZOO" == str);
  }

  writeln("--- composition respects identity");
  {
    // import std.functional : toDelegate;
    auto add_3_1 = identity!(int delegate(int))(x => x + 1);
    debug dump!add_3_1;
    assert(is(typeof(add_3_1) == delegate));
    static assert(isDelegate!add_3_1);
    
    auto add_3_4 = composeV2!(int)(add_3_1, x => x + 3);
    debug dump!add_3_4;

    auto x_3_1 = identity(add_3_4);
    debug dump!x_3_1;
    assert(is(typeof(x_3_1) == delegate));
    assert(isDelegate!x_3_1);

    auto x_3_2 = add_3_4(5);
    debug dump!x_3_2;
    assert(is(typeof(x_3_2) == int));
    assert(x_3_2 == 9);
  }
}
