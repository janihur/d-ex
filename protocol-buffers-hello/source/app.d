import hello;

void separator(in int n){
  import std.format;
  import std.stdio;
  writeln(format("#%d -----", n));
}

auto greet(in Hello h)
pure
{
  import std.format;
  return format("Hello %s !", h.name);
}

auto greet(in HelloList h)
pure
{
  import std.algorithm;
  import std.format;
  return h.name.map!(name => format("Hello %s !", name));
}

void main()
{
  import std.stdio;

  int n = 1;
  separator(n++);
  {
    Hello h;
    h.name = "Jani";

    writeln(greet(h));
  }

  separator(n++);
  {
    HelloList h;
    h.name ~= "Jani";
    h.name ~= "Suresh";
    h.name ~= ["Lendel", "Sami"];

    import std.algorithm;
    import std.array;
    greet(h).array.sort.each!writeln;
  }
}
