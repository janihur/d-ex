Google Protocol Buffers "Hello World" Example
=============================================

https://developers.google.com/protocol-buffers/

First [download and install](https://github.com/protocolbuffers/protobuf/releases) protocol buffer compiler _protoc_.

Second install D [protobuf](https://code.dlang.org/packages/protobuf) package and build protoc D codegen plugin:

```
dub fetch protobuf
cd ~/.dub/packages/protobuf-0.5.0/protobuf
dub build :protoc-gen-d
```

Now the protocol buffer compiler D codegen plugin is available in:

```
~/.dub/packages/protobuf-0.5.0/protobuf/build/protoc-gen-d
```

How to compile a .proto-file:

```
mkdir generated
protoc \
 --plugin=$HOME/.dub/packages/protobuf-0.5.0/protobuf/build/protoc-gen-d \
 --d_opt=message-as-struct \
 --d_out=generated \
 --proto_path . \
 hello.proto
```

The D code is compiled with dub:

```
dub
```
