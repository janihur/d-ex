/*
Stack Overflow answer: https://stackoverflow.com/a/56389206/272735
*/

import std.algorithm.iteration : map;
import std.range : zip;
import std.stdio;

void main() {
  auto list1 = [1, 2, 3, 4];
  auto list2 = [10, 25, 35, 58];

  // Question #2
  auto list3 = zip(list1, list2).map!(a => a[0] * a[1]);

  writeln(list3);

  // Question #1
  typeof(list1) list4;
  foreach(a; zip(list1, list2)) {
    list4 ~= a[0] * a[1];
  }

  writeln(list4);
}
