/**
Calculate the overlapping area of two rectangles.

See:

https://stackoverflow.com/a/7143951/272735
https://stackoverflow.com/a/4549594/272735

Coordinate system:

A = Rectangle(1, 1, 4, 4)
B = Rectangle(3, 4, 3, 2)

 01234657
0
1 AAAA
2 AAAA
3 AAAA
4 AABBB
5   BBB
6
7

*/

struct Rectangle {
  int x; // top left
  int y; // top left
  int width; // from left to right
  int height; // from top to bottom

  int area() const {
    return width * height;
  }
}

Rectangle intersection(Rectangle r1, Rectangle r2)
{
  import std.algorithm.comparison : min, max;
  
  auto left = max(r1.x, r2.x);
  auto right = min(r1.x + r1.width, r2.x + r2.width);
  auto top = max(r1.y, r2.y);
  auto bottom = min(r1.y + r1.height, r2.y + r2.height);

  auto width = right - left;
  auto height = bottom - top;

  if (width <= 0 || height <= 0) {
    return Rectangle();
  }

  return Rectangle(left, top, width, height);
} unittest {
  // import std.stdio : writefln;

  Rectangle r;
  
  r = intersection(Rectangle(1, 1, 4, 4),
                   Rectangle(3, 4, 3, 2));
  // writefln("(r = %s)(r.area = %d)", r, r.area);
  assert(r == Rectangle(3, 4, 2, 1));
  assert(r.area == 2);

  r = intersection(Rectangle(1, 1, 4, 4),
                   Rectangle(6, 2, 3, 1));
  // writefln("(r = %s)(r.area = %d)", r, r.area);
  assert(r == Rectangle());
  assert(r.area == 0);

  r = intersection(Rectangle(1, 1, 4, 4),
                   Rectangle(1, 5, 4, 4));
  // writefln("(r = %s)(r.area = %d)", r, r.area);
  assert(r == Rectangle());
  assert(r.area == 0);

  r = intersection(Rectangle(1, 1, 4, 4),
                   Rectangle(1, 1, 4, 4));
  // writefln("(r = %s)(r.area = %d)", r, r.area);
  assert(r == Rectangle(1, 1, 4, 4));
  assert(r.area == 16);
  
  r = intersection(Rectangle(1, 1, 4, 4),
                   Rectangle(0, 2, 6, 1));
  // writefln("(r = %s)(r.area = %d)", r, r.area);
  assert(r == Rectangle(1, 2, 4, 1));
  assert(r.area == 4);
}
